#####---------------------------------------------------------------------> Mod by Sig "GreatExperiment" Altre

ideas = {

	etgi_ideas_foreign_relations = { 
	
		law = yes
		
		#Sphere of Influence
		etgi_laws_foreign_relations_d = {
			
			modifier = {
			
				opinion_gain_monthly_same_ideology_factor = 0.15
			
			}
			
			cancel_if_invalid = no
			
			ai_will_do = {
			
   				factor = 15
				
   			}
			
		}
		
		#Open Relations
		etgi_laws_foreign_relations_1 = {
			
			modifier = {
			
				research_time_factor = -0.05
				drift_defence_factor = -0.75
			
			}
			
			cancel_if_invalid = no

			ai_will_do = {
			
   				factor = 1

   				modifier = {
				
					factor = 10
					has_government = democratic
					
				}
   			}
		}
		
		#Trade Focused
		etgi_laws_foreign_relations_2 = {
			
			modifier = {
			
				trade_opinion_factor = 0.50
				local_resources_factor = 0.10
				justify_war_goal_time = 0.75
			
			}
			
			cancel_if_invalid = no

			ai_will_do = {
			
   				factor = 1

   			}
		}
		
		#Closed Doors
		etgi_laws_foreign_relations_3 = {
			
			modifier = {
			
				trade_opinion_factor = -0.90
				drift_defence_factor = 0.75
			
			}
			
			cancel_if_invalid = no

			ai_will_do = {
			
   				factor = 1

   			}
		}
	}
}

#####---------------------------------------------------------------------> End